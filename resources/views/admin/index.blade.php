@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>List of Restaurant</h1><br>
        @if(Auth::user()->type == 'A')
        <a href="{{ route('listing.create')}}" class="btn btn-lg btn-primary">Create</a>
        @endif
        <br><br>

        <div class="row">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">List Name</th>
                  <th scope="col">Distance</th>
                  <th scope="col">User</th>
                  <th scope="col">Created At</th>
                  @if(Auth::user()->type == 'A')
                  <th>Action</th>
                  @endif
                </tr>
              </thead>
              <tbody>
                  @foreach ($lists as $list)
                      <tr>
                        <td>
                            <a href="{{ route('listing.show',$list->id)}}">{{ $list->list_name }}</a>
                        </td>
                        <td>{{ $list->distance }}</td>
                        <td>{{ $list->user->name }}</td>
                        <td>{{ $list->created_at }}</td>
                        @if(Auth::user()->type == 'A')
                        <td>
                            <form action="{{ route('listing.destroy',$list->id)}}" method="POST">
                                @csrf
                                @method('delete')

                                <a href="{{ route('listing.edit',$list->id)}}" class="btn btn-sm btn-warning">Edit</a>
                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                            </form>
                        </td>
                        @endif
                      </tr>
                  @endforeach
              </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
        $(function(){

        // handle delete button click
        $('body').on('click', '.button-delete', function(e) {
        e.preventDefault();

        // get the id of the todo task
        var id = $(this).attr('data-id');

        // get csrf token value
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        // now make the ajax request
        $.ajax({
          'url': '/todo/' + id,
          'type': 'DELETE',
          headers: { 'X-CSRF-TOKEN': csrf_token }
        }).done(function() {
          console.log('Todo task deleted: ' + id);
          window.location = window.location.href;
        }).fail(function() {
          alert('something went wrong!');
        });

        });

        });
</script>
@endsection
