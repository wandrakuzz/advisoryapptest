@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Edit list</h2><br>
        <div class="row">
            <form action="{{ route('listing.update',$listing)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
              <div class="form-group">
                <label for="exampleInputEmail1">List Name</label>
                <input name="list_name" type="text" class="form-control" id="list_name" value="{{ $listing->list_name }}" placeholder="Enter restaurant name">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Distance</label>
                <input name="distance" type="number" class="form-control" value="{{ $listing->distance }}" id="distance" placeholder="Distance">
              </div>
              <div class="form-group">
                <select class="" name="">
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}" {{($listing->user->id == $user->id ? 'selected' : '')}}>{{ $user->name }}</option>
                    @endforeach
                </select>
              </div>
              <a href="{{ route('listing.index')}}" class="btn btn-primary">Back</a>
              <button type="submit" class="btn btn-success">Update</button>
            </form>
        </div>
    </div>
@endsection
