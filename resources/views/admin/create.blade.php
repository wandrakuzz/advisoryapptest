@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Create new list</h2><br>
        <div class="row">
            <form action="{{ route('listing.store')}}" method="post" enctype="multipart/form-data">
                @csrf
              <div class="form-group">
                <label for="exampleInputEmail1">List Name</label>
                <input name="list_name" type="text" class="form-control" id="list_name" aria-describedby="emailHelp" placeholder="Enter restaurant name" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Distance</label>
                <input name="distance" type="number" step=".01" class="form-control" id="distance" placeholder="Distance" required>
              </div>
              <div class="form-group">
                <select class="" name="user_id">
                    <option value="" selected disabled>Select</option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
              </div>
              <a href="{{ route('listing.index')}}" class="btn btn-primary">Back</a>
              <button type="submit" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
@endsection
