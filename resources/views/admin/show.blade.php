@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card" style="width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">{{ $listing->list_name }}</h5>
            <p class="card-text text-muted">The restaurant is {{ $listing->distance }} KM from your location.</p>
            <a href="{{ route('listing.index')}}" class="card-link btn-sm btn-primary">Back</a>
            @if(Auth::user()->type == 'A')
            <a href="{{ route('listing.edit',$listing)}}" class="card-link btn-sm btn-warning">Edit</a>
            @endif
          </div>
        </div>
    </div>
@endsection
