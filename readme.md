## advisoryapptest

### Introduction

This is the answer fot he part B in the question.

### Requirement
* PHP 7.2
* MySQL 5.6


### Setup
Clone this repo

    $ git clone https://gitlab.com/wandrakuzz/advisoryapptest.git advisoryapp && cd advisoryapp

### Composer
Install PHP package

    $ composer install

### Setup environment
Copy .env file and setup your own environment - MySQL database, timezone

    $ cp .env.example .env

### Generate key
Generate app key

    $ php artisan key:generate

Generate jwt key

    $ php artisan jwt:secret

### Seed database
Migrate database seed

    $ php artisan migrate --seed

### User login
Admin  - admin@advisoryapp.com

password - secret

User   - user@advisoryapp.com

password - secret

### Api link test
Login -  "/api/login?email=xxx&password=secret"

Listing - "/api/listing?id=1&token=<token here>"

### License

MIT: [http://www.mit-license.org](http://www.mit-license.org)
