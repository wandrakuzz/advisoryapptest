<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = [
            [
            'name' => 'Administrator',
            'email' => 'admin@advisoryapp.com',
            'password' => bcrypt('secret'),
            'type'     => 'A',
            ],
            [
                'name' => 'User',
                'email' => 'user@advisoryapp.com',
                'password' => bcrypt('secret'),
                'type'     => 'U',
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }


    }
}
