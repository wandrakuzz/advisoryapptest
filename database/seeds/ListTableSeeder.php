<?php

use Illuminate\Database\Seeder;
use App\Listing;

class ListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $lists = [
            [
                'list_name' => 'Pantai Seafood Restaurant',
                'distance'  => '1.9',
                'user_id'   => 1
            ],
            [
                'list_name' => 'Signature By The Hill @ The Roof',
                'distance'  => '2.4',
                'user_id'   => 1
            ],
            [
                'list_name' => 'Cinnamon Coffee House',
                'distance'  => '2.6',
                'user_id'   => 2
            ],
            [
                'list_name' => 'Village Park Restaurant',
                'distance'  => '3',
                'user_id'   => 2
            ],
            [
                'list_name' => 'Ticklish Ribs & Wiches',
                'distance'  => '4.2',
                'user_id'   => 1
            ],
            [
                'list_name' => 'myBurgerLab Sunway',
                'distance'  => '7.7',
                'user_id'   => 1
            ],
            [
                'list_name' => 'the Bulb Coffee',
                'distance'  => '2.4',
                'user_id'   => 2
            ],
            [
                'list_name' => 'PappaRich',
                'distance'  => '2.5',
                'user_id'   => 1
            ],
        ];

        foreach ($lists as $list) {
            Listing::create($list);
        }

    }
}
