<?php

namespace App\Http\Controllers;

use App\Listing;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (Auth::user()->type == 'A') {
        //     $lists = Listing::paginate(10);
        // } else {
        //     $lists = Listing::where('user_id',2)->paginate(10);
        // }
        $lists = Listing::paginate(10);

        return view('admin.index',compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();

        return view('admin.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'list_name' => 'required|max:255',
            'distance'  => 'required|numeric|between:0,999.99',
            'user_id'   => 'required'
        ]);

        Listing::create($request->all());

        return redirect()->route('listing.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function show(Listing $listing)
    {
        //
        return view('admin.show',compact('listing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function edit(Listing $listing)
    {
        $users = User::get();

        return view('admin.edit',compact('listing','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listing $listing)
    {
        //
        $this->validate($request,[
            'list_name' => 'required|max:255',
            'distance'  => 'required|numeric|between:0,999.99'
        ]);

        Listing::where('id',$listing->id)->update($request->except('_token','_method'));

        return redirect()->route('listing.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listing $listing)
    {
        //
        Listing::find($listing->id)->delete();

        return redirect()->route('listing.index');
    }
}
