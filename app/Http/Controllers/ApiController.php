<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Listing;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller
{
    //
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $user_id = User::where('email', $request->email)->pluck('id');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'error' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.'], 404);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.'], 500);
        }
        // all good so return the token
        User::where('id',$user_id)->update([
                'token' => $token
            ]);

        return response()->json(['success' => true, 'token' => $token , 'id' => $user_id , 'status' => [ 'code' => 200 , 'message' => 'Access Granted']]);
    }

    public function show(Request $request)
    {
        $listing = Listing::where('user_id', auth()->user()->id)->get(['id','list_name','distance']);

        if (! $listing->count() == 0) {
            return response()->json(['listing' => $listing,'status' => [ 'code' => 200, 'message' => 'Listing successfully retrieved']]);
        }
        return response()->json(['success' => false, 'error' => 'We cant find data with this credentials. Please make sure you entered the right information.'], 404);

    }

    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            User::where('token',JWTAuth::getToken())->update([ 'token' => null ]);
            return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }
}
