<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Listing extends Model
{
    //
    protected $fillable = [
        'list_name',
        'distance',
        'user_id'
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }
}
